﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Calculator;

namespace myTests
{
    public class myTests
    {
        [Fact]
        public void Add_CalculateAddNumberValue()
        {
            //Arrange
            double num1 = 14;
            double num2 = 14;
            double expectedValue = 28;

            //Act
            Calc calc = new Calc();
            double res = calc.Add(num1, num2);

            //Assert
            Assert.Equal(res, expectedValue);
        }

        [Fact]
        public void Substract_CalculateSubstractNumberValue()
        {
            //Arrange
            double num1 = 14;
            double num2 = 14;
            double expectedValue = 0;

            //Act
            Calc calc = new Calc();
            double res = calc.Substract(num1, num2);

            //Assert
            Assert.Equal(res, expectedValue);
        }
        [Fact]
        public void Multiply_CalculateMultiplyNumberValue()
        {
            //Arrange
            double num1 = 14;
            double num2 = 14;
            double expectedValue = 196;

            //Act
            Calc calc = new Calc();
            double res = calc.Multiply(num1, num2);

            //Assert
            Assert.Equal(res, expectedValue);
        }
        [Fact]
        public void Divide_CalculateDivideNumberValue()
        {
            //Arrange   
            double num1 = 14;
            double num2 = 14;
            double expectedValue = 1;

            //Act
            Calc calc = new Calc();
            double res = calc.Divide(num1, num2);

            //Assert
            Assert.Equal(res, expectedValue);
        }
        [Fact]
        public void Add_TestToInfinity()
        {
            //Arrange
            double num1 = double.MaxValue;
            double num2 = double.MaxValue;

            //Act
            Calc calc = new Calc();

            //Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => calc.Add(num1, num2));
        }
        [Fact]
        public void Divide_TestDivideZeroNotAllowed()
        {
            //Arrange
            double num1 = 1;
            double num2 = 0;

            //Act
            Calc calc = new Calc();

            //Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => calc.Divide(num1,num2));
        }
    }
}
